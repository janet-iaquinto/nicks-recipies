#!/usr/bin/env python3

import os
import sys
import shutil
import typing
import collections

import yaml
import click
import jinja2


class Recipie:
    """ Holds the information for one recipie
    """

    def __init__(self,
                 title: str,
                 hierarchy: typing.Iterable[str],
                 recipies_dir: str='recipies',
                 data: typing.Dict=None):
        self.__title = title
        self.__hierarchy = hierarchy

        self.__data = data
        if data is None:
            filepath = os.path.join(*(
                [recipies_dir] + self.__hierarchy + ['{}.yaml'.format(title)]))
            with open(filepath) as f:
                self.__data = yaml.load(f.read())

    @property
    def hierarchy(self) -> typing.Iterable[str]:
        return self.__hierarchy

    @property
    def ingredients(self) -> typing.Dict:
        i = self.__data.get('ingredients', {})
        return collections.OrderedDict(sorted(i.items()))

    @property
    def steps(self) -> typing.Iterable[str]:
        return self.__data.get('steps', [])

    @property
    def servings(self) -> typing.Optional[int]:
        return self.__data.get('servings', None)

    @property
    def description(self) -> typing.Optional[str]:
        return self.__data.get('description', None)

    @property
    def title(self) -> str:
        return self.__title

    @property
    def dict(self):
        return {
            'title': self.title,
            'steps': self.steps,
            'servings': self.servings,
            'hierarchy': self.hierarchy,
            'ingredients': self.ingredients,
            'description': self.description,
        }


class IndexEntry:
    """ An entry in the list on an index page, ex. a folder or recipie.
    """

    def __init__(
        self,
        title: str,
        link: str,
        description: str=None,
        children: typing.Iterable=None,
    ):
        self.__link = link
        self.__title = title
        self.__description = description
        self.__children = children if children is not None else []

    @property
    def link(self) -> str:
        return self.__link

    @property
    def title(self) -> str:
        return self.__title

    @property
    def description(self) -> str:
        return self.__description

    @property
    def children(self):
        return self.__children

    @property
    def dict(self):
        return {
            'link': self.link,
            'title': self.title,
            'description': self.description,
            'children': [c.dict for c in self.children],
        }


Breadcrumb = collections.namedtuple('Breadcrumb', ('title', 'link'))
Breadcrumb.dict = lambda self: {'title': self.title, 'link': self.link}


class Theme:
    """ A collection of HTML templates for formatting the recipies into HTML
    """

    RECIPIE_TEMPLATE = 'recipie.html'
    INDEX_TEMPLATE = 'index.html'

    def __init__(self, theme_dir: str):
        """ Setup a Jinja2 environment from the passed theme directory
        """
        self.__theme_dir = theme_dir
        loader = jinja2.FileSystemLoader(self.__theme_dir)
        self.__jinja2_env = jinja2.Environment(loader=loader)

    @property
    def theme_dir(self) -> str:
        return self.__theme_dir

    @property
    def static_files(self) -> typing.Iterable[str]:
        """ List of static files, relative to the theme root
        """
        return ['static/main.css']

    def render_index(
        self,
        title: str,
        entries: typing.Iterable[IndexEntry],
        breadcrumbs: typing.Iterable[Breadcrumb]=None,
    ) -> str:
        """ Render HTML for an index page, ex. for a course, ingredient, etc.
        """
        tmpl = self.__jinja2_env.get_template(self.INDEX_TEMPLATE)
        return tmpl.render(
            title=title,
            breadcrumbs=breadcrumbs,
            entries=[e.dict for e in entries],
            relative_root=get_relative_root(breadcrumbs))

    def render_recipie(self, recipie: Recipie) -> str:
        """ Render HTML for a single recipie
        """
        breadcrumbs = get_relative_breadcrumbs(
                ['Cookbook!'] + recipie.hierarchy, offset=1)

        tmpl = self.__jinja2_env.get_template(self.RECIPIE_TEMPLATE)
        return tmpl.render(
            relative_root=get_relative_root(breadcrumbs, offset=1),
            recipie=recipie.dict,
            breadcrumbs=breadcrumbs)


def get_relative_root(breadcrumbs: typing.Iterable, offset: int=0) -> str:
    relative_root = '/'.join(['..' for _ in range(len(breadcrumbs) - offset)])
    if relative_root != '':
        relative_root += '/'
    return relative_root


def get_recipies(
    recipies_dir: str,
    dir_path: str=None,
    hierarchy: typing.Iterable[str]=None,
) -> typing.Generator[None, Recipie, None]:
    """ For a recipie directory, return all sub-directories (ie. the courses)
    """
    if hierarchy is None:
        hierarchy = []

    if dir_path is None:
        dir_path = recipies_dir

    for name in os.listdir(dir_path):
        path = os.path.join(dir_path, name)

        # Recurse directories
        if os.path.isdir(path):
            yield from get_recipies(recipies_dir, path, hierarchy + [name])

        # YAML Files are recipies
        if os.path.isfile(path) and len(name) > 5 and name[-5:] == '.yaml':
            yield Recipie(name[:-5], hierarchy, recipies_dir=recipies_dir)

        # TODO: Handle unknown non-dir, non-file


def create_dir(output_dir: str):
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)


def get_relative_breadcrumbs(hierarchy: typing.Iterable[str], offset: int=0):
    breadcrumbs = []
    for index, title in enumerate(hierarchy):
        relative = ['..' for _ in range(
            len(hierarchy) - index - offset)]
        link = '/'.join(relative + ['index.html'])
        breadcrumbs.append(Breadcrumb(title, link))
    return breadcrumbs


def write_recipie_html(output_dir: str, recipie: Recipie, html: str):
    dir_path = output_dir
    for title in recipie.hierarchy:
        dir_path = os.path.join(dir_path, title.lower())
        create_dir(dir_path)

    recipie_path = os.path.join(dir_path, '{}.html'.format(recipie.title))
    with open(recipie_path, 'w') as f:
        f.write(html)


def index_entries_from_recipie_tree(
    recipie_tree: typing.Dict,
) -> typing.Generator[None, IndexEntry, None]:
    for key, value in sorted(recipie_tree.items()):
        # Handle directories
        if isinstance(value, dict):
            yield IndexEntry(key, '{}/index.html'.format(key.lower()))

        # Handle recipies
        if isinstance(value, Recipie):
            yield IndexEntry(
                value.title,
                '{}.html'.format(value.title),
                description=value.description)

        # TODO: Handle invalid values in tree


def write_indexes(
    output_dir: str,
    theme: Theme,
    title: str,
    recipies_index: typing.Dict,
    hierarchy: typing.Iterable=None,
):
    """ Write out the home page, courses pages, and main ingredient pages

    Arguments:
        output_dir (str): Directory to write out all the HTML
        theme (str): The Theme instance, used in rendering recipies
        title (str): Title of this index page

    Keyword Arguments:
        hierarchy (iter): Iterable of index page titles
    """
    recurse = True
    if hierarchy is None:
        hierarchy = []
        recurse = False

    # Write out the home page as a top-level index
    entries = [e for e in index_entries_from_recipie_tree(recipies_index)]
    breadcrumbs = get_relative_breadcrumbs(hierarchy)
    html = theme.render_index(title, entries, breadcrumbs)

    # Build the index file path
    path_parts = [output_dir]
    if len(hierarchy):
        path_parts.extend([h.lower() for h in hierarchy[1:]])
    if recurse:
        path_parts.append(title.lower())
    index_path = os.path.join(*(path_parts + ['index.html']))

    # Actually write out the index html to file
    with open(index_path, 'w') as f:
        f.write(html)

    click.echo('Rendered Index: {}'.format(' > '.join(hierarchy + [title])))

    # Recurse into sub-directories
    for key, value in recipies_index.items():
        if isinstance(value, dict):
            write_indexes(output_dir, theme, key, value, hierarchy + [title])


def write_recipies(
    recipies_dir: str,
    output_dir: str,
    theme: Theme
) -> typing.Dict:
    """ Write the actual recipie files (at the bottom of the tree)
    This also builds the index of recipies by Course > Main Ingredient

    Arguments:
        recipies_dir (str): Directory where recipie yaml files are
        output_dir (str): Directory to write out all the HTML
        theme (Theme): The Theme instance, used in rendering recipies

    Returns:
        dict: The index of recipies by course, then main ingredient
    """
    recipies_tree = {}

    # Load each recipie from disk into a python object
    for recipie in get_recipies(recipies_dir):
        # Render each recipie and write it to disk in the output directory
        html = theme.render_recipie(recipie)
        write_recipie_html(output_dir, recipie, html)
        click.echo('Rendered recipie: {} > {}'.format(
            ' > '.join(recipie.hierarchy),
            recipie.title))

        # TODO: Add recipie to the tree
        tree_ref = recipies_tree
        for title in recipie.hierarchy:
            if tree_ref.get(title) is None:
                tree_ref[title] = {}
            tree_ref = tree_ref[title]
        tree_ref[recipie.title] = recipie

    return recipies_tree


def write_static(output_dir: str, theme: Theme):
    """ Copy all the static files to the output directory

    Arguments:
        output_dir (str): Directory to write out all the HTML
        theme (Theme): The Theme instance, used in rendering recipies
    """
    for filepath in theme.static_files:
        path_parts = filepath.split('/')
        if len(path_parts) > 1:
            dir_path = output_dir
            for dirname in path_parts[:1]:
                dir_path = os.path.join(dir_path, dirname)
                if not os.path.exists(dir_path):
                    os.mkdir(dir_path)

        src = os.path.join(theme.theme_dir, filepath)
        dest = os.path.join(output_dir, filepath)

        if os.path.exists(dest):
            os.remove(dest)
        shutil.copyfile(src, dest)


@click.group()
def cli():
    pass


@click.command()
@click.option('-r', '--recipies-dir', type=str, default='recipies')
@click.option('-t', '--theme-dir', type=str, default='theme')
@click.option('-o', '--output-dir', type=str, default='html')
def render(recipies_dir, theme_dir, output_dir):
    """ Load all the recipies and render them to HTML in the output directory

    Arguments:
        recipies_dir (str): Directory where recipie yaml files are
        theme_dir (str): Directory containing the theme to use
        output_dir (str): Directory to write out all the HTML
    """
    # Load the HTML theme
    theme = Theme(theme_dir)

    # Make sure the top-level HTML output directory exists
    if os.path.exists(output_dir):
        shutil.rmtree(output_dir)
    create_dir(output_dir)

    # Create all the recipies and index pages (ie. Home, Course, Ingredient)
    recipies_index = write_recipies(recipies_dir, output_dir, theme)
    write_indexes(output_dir, theme, 'Cookbook!', recipies_index)

    # Copy all the static files to the output directory
    write_static(output_dir, theme)

    return sys.exit(0)


BLANK_RECIPIE = """description: This is an awesome, new recipie
ingredients:
  'Example':
    amount: 1
    units: Cups
steps:
  - Example step 1
servings: 1"""


@click.command('new')
@click.option('-p', '--path', type=str, default='recipies')
@click.option('-f', '--force', is_flag=True)
@click.argument('title', type=str)
def new_recipie(title, path, force):
    """ Create a new (empty) recipie

    Arguments:
        title (str)
        path (str)
    """
    # Check if the file already exists
    full_path = os.path.join(path, '{}.yaml'.format(title))
    if os.path.exists(full_path) and not force:
        return sys.exit(
            'File "{}" already exists, either delete it or overwrite '
            'it by running this command again with `-f`.'.format(full_path))

    # Create the path (ie. folder) if it doesn't exist
    path_parts = path.split('/')
    if not os.path.exists(path):
        path = ''
        for dirname in path_parts:
            path = os.path.join(path, dirname)
            create_dir(path)

    # Create the recipie
    with open(full_path, 'w') as f:
        f.write(BLANK_RECIPIE)

    click.echo('Created new recipie: {}'.format(full_path))
    return sys.exit(0)


cli.add_command(render)
cli.add_command(new_recipie)

if __name__ == '__main__':
    cli()
